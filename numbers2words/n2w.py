# -*- coding: utf-8 -*-
__author__ = 'Radek Hofman'

import abc
import unittest

"""
Simple program for translation of integers into words.
Currently, two languages are supported: English and German
Each language has its own specific word order.

Classes for respective languages are inherited from a
generic class NumberDictionary and implement abstract method put_number_together(self, *args)
of NumberDictionary for custom word order in different languages.

Objects of dictionaries are callable, so one can, e.g., do:

>>> translator = EnglishDictionary(numbers_dict["en"], orders_dict["en"], "English")
>>> translator(113)

Numbers_dict[lang] and orders_dict[lang] are dictionaries for a given language.
"""

numbers_en = {0: "zero",
            1: "one",
            2: "two",
            3: "three",
            4: "four",
            5: "five",
            6: "six",
            7: "seven",
            8: "eight",
            9: "nine",
            10: "ten",
            11: "eleven",
            12: "twelve",
            13: "thirteen",
            14: "fourteen",
            15: "fifteen",
            16: "sixteen",
            17: "seventeen",
            18: "eighteen",
            19: "nineteen",
            20: "twenty",
            30: "thirty",
            40: "forty",
            50: "fifty",
            60: "sixty",
            70: "seventy",
            80: "eighty",
            90: "ninety"}

numbers_de = {0: "Null",
            1: "eins",
            2: "zwei",
            3: "drei",
            4: "vier",
            5: "fünf",
            6: "sechs",
            7: "sieben",
            8: "acht",
            9: "neun",
            10: "zehn",
            11: "elf",
            12: "zwölf",
            13: "dreizehn",
            14: "vierzehn",
            15: "fünfzehn",
            16: "sechszehn",
            17: "siebzehn",
            18: "achtzehn",
            19: "neunzehn",
            20: "zwanzig",
            30: "dreißig",
            40: "vierzig",
            50: "fünfzig",
            60: "sechzig",
            70: "siebzig",
            80: "achtzig",
            90: "neunzig"}

orders_en = {1e12: "trillion",
          1e9 : "billion",
          1e6 : "million",
          1e3 : "thousand",
          1e2 : "hundred",
          "-" : "minus"}

orders_de = {1e12: "Trillion",
          1e9 : "Billion",
          1e6 : "Million",
          1e3 : "tausend",
          1e2 : "hundert",
          "-" : "minus"}

numbers_dict = {"en": numbers_en,
                "de": numbers_de}

orders_dict = {"en": orders_en,
               "de": orders_de}


class Test(unittest.TestCase):
    """ Unit test for "integer to words" program """

    def setUp(self):
        """Set-up for tests - creation of translator objects"""
        self.translator_en = EnglishDictionary(numbers_dict["en"], orders_dict["en"], "English")
        self.translator_de = GermanDictionary(numbers_dict["de"], orders_dict["de"], "German")

    def test_532_en(self):
        self.assertEqual(self.translator_en(532), "five hundred thirty two")

    def test_zero_en(self):
        self.assertEqual(self.translator_en(0), "zero")

    def test_negative_en(self):
        self.assertEqual(self.translator_en(-12105678), "minus twelve million one hundred five thousand six hundred seventy eight")

    def test_trillione_en(self):
        self.assertEqual(self.translator_en(1000000000000), "one trillion")

    def test_large_number_en(self):
        self.assertEqual(self.translator_en(100000005000), "one hundred billion five thousand")

    def test_532_de(self):
        self.assertEqual(self.translator_de(532), "fünf hundert zwei und dreißig")

    def test_zero(self):
        self.assertEqual(self.translator_de(0), "Null")

    def test_negative_de(self):
        self.assertEqual(self.translator_de(-12105678), "minus zwölf Million eins hundert fünf tausend sechs hundert acht und siebzig")

    def test_trillion_de(self):
        self.assertEqual(self.translator_de(1000000000000), "eins Trillion")

    def test_large_number_de(self):
        self.assertEqual(self.translator_de(100000005000), "eins hundert Billion fünf tausend")


class NumberDictionary(object):
    """
    Class implementing a number dictionary
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self,numbers_dict, orders_dict, lang):
        self.numbers = numbers_dict
        self.orders = orders_dict
        self.lang = lang

    def parse1000(self, number):
        """
        Parses numbers from 0-999.
            number - an integer to parse
        """

        s = []
        if 0 <= number < 1000:
            if number in self.numbers.keys():
                s.append(self.numbers[number])
                return s
            else:
                rem100 = (number/100)  # are hundreds present?
                number -= (number/100)*100
                rem10 = (number/10)*10    # are tens present?
                rem1 = number % 10  # are units present?

                #we know which orders are present with which magnitude, the following method
                #puts together words in correct word order for different languages
                s += self.put_number_together(rem100, rem10, rem1)
                return s
        else:
            raise ValueError("Number must be between 0 and 999!")

    def parse_number(self, number0):
        """
         Translates integer number to words.
         Let's limit ourselves to integers from -1e12 - 1e12.
            number0: - integer to parse
        """

        number = number0  # we want to keep original number

        #type check of input
        if not isinstance(number, int):
            raise TypeError("Integer input expected, got %s!" % type(number))

        #range check of input
        if -1e12 <= number <= 1e12:  # test of validity of input
            s = []  # words will be stored in a list

            #we need special treatment of negative numbers
            if number < 0:
                s.append(self.orders["-"])
                number = abs(number)

            if number in self.numbers.keys():  # number already present in our dictionary
                s.append(self.numbers[number])
            else:  # number must be parsed
                for i in range(12, -1, -3):  # we iterate over multiplies of 1e3
                    factor = 10**i
                    rem = number/factor
                    if rem > 0:  # yes, the order is present
                        s += self.parse1000(rem)+[self.orders.get(factor, "")]  # parsing current order magnitude
                        number -= rem*factor  # subtraction of already parsed parts

            words = " ".join(filter(lambda x: x, s))  # remove empty fields from word list
            print number0, "'%s'" % words
            return words

        else:
            raise ValueError("Number must be between -1e12 and 1e12")

    def __call__(self, number):
        return self.parse_number(number)

    @abc.abstractmethod
    def put_number_together(self, *args):
        """
        A custom implementation of treatment of word order for different languages
        Puts together words for different languages with possible different word order...

        This method is abstract and thus has to be implemented when instantiated
        """
        return


class GermanDictionary(NumberDictionary):
    """
    Class for German language with custom definition of word order
    """

    def put_number_together(self, *args):
        rem100, rem10, rem1 = args
        s = [self.numbers[rem100] if rem100 > 0 else "",
             self.orders[1e2] if rem100 > 0 else "",
             self.numbers[rem1] if rem1 > 0 else "",
             "und" if rem10 > 0 else "",
             self.numbers[rem10] if rem10 > 0 else ""
             ]
        return s


class EnglishDictionary(NumberDictionary):
    """
    Class for English language with custom definition of word order
    """

    def put_number_together(self, *args):
        rem100, rem10, rem1 = args
        s = [self.numbers[rem100] if rem100 > 0 else "",
             self.orders[1e2] if rem100 > 0 else "",
             self.numbers[rem10] if rem10 > 0 else "",
             self.numbers[rem1] if rem1 > 0 else ""]
        return s


def main():
    """
    Main runs a unit test for both languages
    """
    unittest.main()


if __name__ == "__main__":
    main()

